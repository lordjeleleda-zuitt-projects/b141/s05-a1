SHOW DATABASES;

USE classic_models;

SHOW TABLES;

1. SELECT customerName FROM customers WHERE country = "Philippines";

2. SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

3. SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

4. SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

5. SELECT customerName FROM customers WHERE state IS NULL;

6. SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

7. SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

8. SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%"; 

9. SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

10. SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

11. SELECT DISTINCT country FROM customers;

12. SELECT DISTINCT status FROM orders;

13. SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

14. SELECT firstName, lastName, city FROM employees JOIN offices ON employees.officeCode = offices.officeCode WHERE city = "Tokyo";

15. SELECT customerName FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE firstName= "Leslie" AND lastName = "Thompson";

16. SELECT productName, customerName FROM customers JOIN orders ON customers.customerNumber= orders.customerNumber JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber JOIN products ON orderdetails.productCode = products.productCode WHERE customerName = "Baane Mini Imports";

17. SELECT firstName, lastName, customerName, offices.country FROM employees JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber JOIN offices ON employees.officeCode = offices.officeCode;

18. SELECT lastName, firstName FROM employees WHERE reportsTo = 1143;

19. SELECT productName, MSRP FROM products ORDER BY MSRP DESC LIMIT 1;

20. SELECT COUNT(*) FROM customers WHERE country = "UK";

21. SELECT COUNT(*) FROM products JOIN productlines ON products.productLine = productlines.productLine;

22. SELECT COUNT(*) FROM customers JOIN employees ON employees.employeeNumber = customers.salesRepEmployeeNumber;

23. SELECT productName, quantityInStock FROM products JOIN productlines ON products.productLine = productlines.productLine WHERE products.productLine = "Planes" AND quantityInStock < 1000;